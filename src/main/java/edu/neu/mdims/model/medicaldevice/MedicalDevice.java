/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.medicaldevice;

import edu.neu.mdims.model.core.Entity;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author NicolasZHANG
 */
public class MedicalDevice extends Entity {

    private static final AtomicInteger count = new AtomicInteger(0);

    private String name;

    private MedicalDeviceClass deviceClass;

    private MedicalDeviceSpeciality speciality;

    private String description;

    private String serialNumber;

    private String powerRequirements;

    private String operationAndServiceRequirements;

    private String lotNumber;

    private String softwareAndFirmwareVersionNumber;

    private String manufacturer;

    private String yearOfManufacture;

    private String expectedDeviceLifetime;

    private double price;

    private final ArrayList<MedicalDeviceProblem> problems;

    private final ArrayList<MedicalDeviceMaintenanceLog> maintenanceLogs;

    public MedicalDevice() {
        super(count);
        problems = new ArrayList<>();
        maintenanceLogs = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MedicalDeviceClass getDeviceClass() {
        return deviceClass;
    }

    public void setDeviceClass(MedicalDeviceClass deviceClass) {
        this.deviceClass = deviceClass;
    }

    public MedicalDeviceSpeciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(MedicalDeviceSpeciality speciality) {
        this.speciality = speciality;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getPowerRequirements() {
        return powerRequirements;
    }

    public void setPowerRequirements(String powerRequirements) {
        this.powerRequirements = powerRequirements;
    }

    public String getOperationAndServiceRequirements() {
        return operationAndServiceRequirements;
    }

    public void setOperationAndServiceRequirements(String operationAndServiceRequirements) {
        this.operationAndServiceRequirements = operationAndServiceRequirements;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public String getSoftwareAndFirmwareVersionNumber() {
        return softwareAndFirmwareVersionNumber;
    }

    public void setSoftwareAndFirmwareVersionNumber(String softwareAndFirmwareVersionNumber) {
        this.softwareAndFirmwareVersionNumber = softwareAndFirmwareVersionNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(String yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    public String getExpectedDeviceLifetime() {
        return expectedDeviceLifetime;
    }

    public void setExpectedDeviceLifetime(String expectedDeviceLifetime) {
        this.expectedDeviceLifetime = expectedDeviceLifetime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<MedicalDeviceProblem> getProblems() {
        return problems;
    }

    public ArrayList<MedicalDeviceMaintenanceLog> getMaintenanceLogs() {
        return maintenanceLogs;
    }

}
