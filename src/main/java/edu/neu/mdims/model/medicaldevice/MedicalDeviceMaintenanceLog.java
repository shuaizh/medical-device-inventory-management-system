/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.medicaldevice;

import edu.neu.mdims.model.core.Entity;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author NicolasZHANG
 */
public class MedicalDeviceMaintenanceLog extends Entity {

    private static final AtomicInteger count = new AtomicInteger(0);

    public MedicalDeviceMaintenanceLog() {
        super(count);
    }
}
