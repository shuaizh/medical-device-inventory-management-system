/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.medicaldevice;

/**
 *
 * @author NicolasZHANG
 */
public enum MedicalDeviceClass {

    ClassI,
    ClassII,
    ClassIII
}
