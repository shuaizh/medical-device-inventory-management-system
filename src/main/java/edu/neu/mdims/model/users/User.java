/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.users;

import edu.neu.mdims.model.core.Entity;
import edu.neu.mdims.model.organizations.Organization;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author NicolasZHANG
 */
public abstract class User extends Entity {

    private static final AtomicInteger count = new AtomicInteger(0);

    private String name;

    private UserRole userRole;

    private String userName;

    private String password;

    private Organization organization;

    private boolean disabled;

    /**
     *
     * @param userRole
     */
    public User(UserRole userRole) {
        super(count);
        this.userRole = userRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

}
