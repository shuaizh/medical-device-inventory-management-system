/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.users;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author NicolasZHANG
 */
public class UserFactory {

    static final Logger logger = LogManager.getLogger(UserFactory.class.getName());

    public static User generateUser(UserRole userRole) {
        User user = null;
        switch (userRole) {
            case NetworkAdmin:
                user = new NetworkAdmin();
                break;
            case WarehouseManager:
                user = new WarehouseManager();
                break;
            case SupplierAdmin:
                user = new SupplierAdmin();
                break;
            case MedicalServiceProviderAdmin:
                user = new MedicalServiceProviderAdmin();
                break;
            case Doctor:
                user = new Doctor();
                break;
            case Nurse:
                user = new Nurse();
                break;
            case OperationManager:
                user = new OperationManager();
                break;
            default:
                logger.error("Illegal user role");
        }
        return user;
    }
}
