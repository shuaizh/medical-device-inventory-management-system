/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.users;

/**
 *
 * @author NicolasZHANG
 */
public enum UserRole {

    NetworkAdmin,
    WarehouseManager,
    SupplierAdmin,
    MedicalServiceProviderAdmin,
    Doctor,
    Nurse,
    OperationManager
}
