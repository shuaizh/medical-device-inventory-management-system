/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.organizations;

import edu.neu.mdims.model.core.Entity;
import java.util.concurrent.atomic.AtomicInteger;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 *
 * @author NicolasZHANG
 */
public abstract class Organization extends Entity {

    private static final AtomicInteger count = new AtomicInteger(0);

    private String name;

    private OrganizationType organizationType;

    private GeoPosition location;

    public Organization(OrganizationType organizationType) {
        super(count);
        this.organizationType = organizationType;
    }

    public Organization(String name, OrganizationType organizationType) {
        this(organizationType);
        this.name = name;
        this.organizationType = organizationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrganizationType getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(OrganizationType organizationType) {
        this.organizationType = organizationType;
    }

    public GeoPosition getLocation() {
        return location;
    }

    public void setLocation(GeoPosition location) {
        this.location = location;
    }

}
