/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.organizations;

import edu.neu.mdims.model.medicaldevice.MedicalDevice;
import java.util.ArrayList;

/**
 *
 * @author NicolasZHANG
 */
public class Supplier extends Organization {

    private final ArrayList<MedicalDevice> medicalDevices;

    private boolean isEnrolled;

    public Supplier() {
        super(OrganizationType.Supplier);
        medicalDevices = new ArrayList<>();
    }

    public Supplier(String name) {
        this();
        setName(name);
    }

    public ArrayList<MedicalDevice> getMedicalDevices() {
        return medicalDevices;
    }

    public boolean isIsEnrolled() {
        return isEnrolled;
    }

    public void setIsEnrolled(boolean isEnrolled) {
        this.isEnrolled = isEnrolled;
    }

}
