/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.organizations;

/**
 *
 * @author NicolasZHANG
 */
public enum OrganizationType {

    Network,
    MedicalServiceProvider,
    Supplier,
    Warehouse
}
