/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.organizations;

import edu.neu.mdims.model.medicaldevice.MedicalDevice;
import edu.neu.mdims.model.users.User;
import java.util.ArrayList;

/**
 *
 * @author NicolasZHANG
 */
public class Network extends Organization {

    private static Network instance;

    private final ArrayList<MedicalServiceProvider> medicalServiceProviders;

    private final ArrayList<MedicalDevice> medicalDevices;

    private final ArrayList<Supplier> suppliers;

    private final ArrayList<Warehouse> warehouses;

    private final ArrayList<User> users;

    private Network() {
        super(OrganizationType.Network);
        medicalServiceProviders = new ArrayList<>();
        medicalDevices = new ArrayList<>();
        suppliers = new ArrayList<>();
        warehouses = new ArrayList<>();
        users = new ArrayList<>();
    }

    public static Network getInstance() {
        if (instance == null) {
            instance = new Network();
        }

        return instance;
    }

    public ArrayList<MedicalServiceProvider> getMedicalServiceProviders() {
        return medicalServiceProviders;
    }

    public ArrayList<MedicalDevice> getMedicalDevices() {
        return medicalDevices;
    }

    public ArrayList<Supplier> getSuppliers() {
        return suppliers;
    }

    public ArrayList<Warehouse> getWarehouses() {
        return warehouses;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public User authenticateUserAccount(String userName, String password) {
        for (User user : users) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }
}
