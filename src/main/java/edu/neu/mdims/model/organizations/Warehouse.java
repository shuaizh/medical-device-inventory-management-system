/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.organizations;

import edu.neu.mdims.model.medicaldevice.MedicalDevice;
import java.util.ArrayList;

/**
 *
 * @author NicolasZHANG
 */
public class Warehouse extends Organization {

    private final ArrayList<MedicalDevice> medicalDevices;

    public Warehouse() {
        super(OrganizationType.Warehouse);
        medicalDevices = new ArrayList<>();
    }

    public Warehouse(String name) {
        this();
        setName(name);
    }

    public ArrayList<MedicalDevice> getMedicalDevices() {
        return medicalDevices;
    }

}
