/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.model.core;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author NicolasZHANG
 */
public abstract class Entity implements Serializable {

    private final int id;

    private final Date created;

    public Entity(AtomicInteger count) {
        id = count.incrementAndGet();
        created = new Date();
    }

    public int getId() {
        return id;
    }

    public Date getCreated() {
        return created;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

}
