/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.utils.validation;

import java.awt.Color;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author NicolasZHANG
 */
public abstract class BaseInputVerifier extends InputVerifier {

    protected JLabel validationMessageLabel;

    public BaseInputVerifier(JLabel validationMessageLabel) {
        this.validationMessageLabel = validationMessageLabel;
    }

    @Override
    public boolean verify(JComponent input) {
        JTextField jtf;
        try {
            jtf = (JTextField) input;
        } catch (ClassCastException e) {
            throw new ClassCastException("All custom verifiers can only be used onto JTextField component.");
        }

        final String inputText = jtf.getText();
        final VerificationResult verificationResult = verify(inputText);
        if (verificationResult.isSuccess()) {
            validationMessageLabel.setText("");
            jtf.setBackground(Color.white);
            return true;
        } else {
            validationMessageLabel.setText(verificationResult.getErrorMessage());
            jtf.setBackground(Color.red);
            return false;
        }
    }

    /**
     * Custom verification method
     *
     * @return VerificationResult
     */
    public abstract VerificationResult verify(String input);
}
