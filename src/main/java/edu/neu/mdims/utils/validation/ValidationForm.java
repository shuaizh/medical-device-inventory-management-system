/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.utils.validation;

import java.util.LinkedHashMap;
import javax.swing.InputVerifier;
import javax.swing.JTextField;

/**
 * A group that contains all the verifiers in a view
 *
 * @author NicolasZHANG
 */
public class ValidationForm {

    private final LinkedHashMap<JTextField, InputVerifier> validationPairs = new LinkedHashMap<>();

    public void addTextField(JTextField jTextField, BaseInputVerifier inputVerifier) {
        jTextField.setInputVerifier(inputVerifier);
        validationPairs.put(jTextField, inputVerifier);
    }

    public boolean isValid() {
        // traverse all the components in the validation form and verify each of them with their designated inputverfifier
        for (JTextField tf : validationPairs.keySet()) {
            InputVerifier iv = validationPairs.get(tf);
            if (!iv.verify(tf)) {
                tf.requestFocus();
                return false;
            }
        }
        return true;
    }
}
