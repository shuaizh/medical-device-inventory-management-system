/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.utils.validation;

import javax.swing.JLabel;

/**
 *
 * @author NicolasZHANG
 */
public class IntegerRangeInputVerifier extends BaseInputVerifier {

    private final int minValue, maxValue;

    public IntegerRangeInputVerifier(JLabel validationMessageLabel, int minValue, int maxValue) {
        super(validationMessageLabel);
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public VerificationResult verify(String input) {
        VerificationResult vr = new VerificationResult();
        try {
            int number = Integer.parseInt(input);
            if (number >= minValue && number <= maxValue) {
                vr.setSuccess(true);
            } else {
                vr.setSuccess(false);
                vr.setErrorMessage("You have to enter a integer number ranged from " + minValue + " to " + maxValue + ".");
            }
        } catch (NumberFormatException nfe) {
            vr.setSuccess(false);
            vr.setErrorMessage("You have to enter a valid integer number.");
        }
        return vr;
    }
}
