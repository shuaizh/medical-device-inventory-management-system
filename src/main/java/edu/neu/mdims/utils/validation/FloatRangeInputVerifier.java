/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.utils.validation;

import javax.swing.JLabel;

/**
 *
 * @author NicolasZHANG
 */
public class FloatRangeInputVerifier extends BaseInputVerifier {

    private final float minValue, maxValue;

    public FloatRangeInputVerifier(JLabel validationMessageLabel, float minValue, float maxValue) {
        super(validationMessageLabel);
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public VerificationResult verify(String input) {
        VerificationResult vr = new VerificationResult();
        try {
            float number = Float.parseFloat(input);
            if (number >= minValue && number <= maxValue) {
                vr.setSuccess(true);
            } else {
                vr.setSuccess(false);
                vr.setErrorMessage("You have to enter a float number ranged from " + minValue + " to " + maxValue + ".");
            }
        } catch (NumberFormatException nfe) {
            vr.setSuccess(false);
            vr.setErrorMessage("You have to enter a valid float number.");
        }
        return vr;
    }
}
