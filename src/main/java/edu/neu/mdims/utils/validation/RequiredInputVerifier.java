/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.utils.validation;

import javax.swing.JLabel;

/**
 *
 * @author NicolasZHANG
 */
public class RequiredInputVerifier extends BaseInputVerifier {

    public RequiredInputVerifier(JLabel validationMessageLabel) {
        super(validationMessageLabel);
    }

    @Override
    public VerificationResult verify(String input) {
        VerificationResult vr = new VerificationResult();
        if ("".equals(input.trim())) {
            vr.setSuccess(false);
            vr.setErrorMessage("You have to fill this field.");
        } else {
            vr.setSuccess(true);
        }
        return vr;
    }
}
