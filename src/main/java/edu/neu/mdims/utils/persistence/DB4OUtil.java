package edu.neu.mdims.utils.persistence;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.ext.DatabaseFileLockedException;
import com.db4o.ext.DatabaseReadOnlyException;
import com.db4o.ext.Db4oIOException;
import com.db4o.ext.IncompatibleFileFormatException;
import com.db4o.ext.OldFormatException;
import com.db4o.ta.TransparentPersistenceSupport;
import edu.neu.mdims.utils.ApplicationInitializer;
import edu.neu.mdims.model.organizations.Network;

public class DB4OUtil {

    private static final String FILENAME = "mdims_db.db4o"; // path to the data store
    private static DB4OUtil dB4OUtil;

    public synchronized static DB4OUtil getInstance() {
        if (dB4OUtil == null) {
            dB4OUtil = new DB4OUtil();
        }
        return dB4OUtil;
    }

    protected synchronized static void shutdown(ObjectContainer conn) {
        if (conn != null) {
            conn.close();
        }
    }

    private ObjectContainer createConnection() {
        try {
            EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
            config.common().add(new TransparentPersistenceSupport());
            //Controls the number of objects in memory
            config.common().activationDepth(Integer.MAX_VALUE);
            //Controls the depth/level of updation of Object
            config.common().updateDepth(Integer.MAX_VALUE);

            //Register your top most Class here
//            config.common().objectClass(Network.class).cascadeOnUpdate(true); // Change to the object you want to save
            ObjectContainer db = Db4oEmbedded.openFile(config, FILENAME);
            return db;
        } catch (Db4oIOException | DatabaseFileLockedException | IncompatibleFileFormatException | OldFormatException | DatabaseReadOnlyException ex) {
            System.out.print(ex.getMessage());
        }
        return null;
    }

    public synchronized void storeNetwork(Network network) {
        ObjectContainer conn = createConnection();

        ObjectSet<Network> networks = conn.query(Network.class);

        if (networks.size() > 0) {
            for (Network es : networks) {
                conn.delete(es);
            }
            conn.commit();
        }

        conn.store(network);
        conn.commit();
        conn.close();
    }

    public Network retrieveNetwork() {
        ObjectContainer conn = createConnection();
        ObjectSet<Network> networks = conn.query(Network.class);
        Network network;
        if (networks.size() == 0) {
            network = ApplicationInitializer.initialize();
        } else {
            network = networks.get(0);
        }
        conn.close();
        return network;
    }
}
