/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.utils;

import edu.neu.mdims.model.organizations.Organization;
import edu.neu.mdims.model.medicaldevice.MedicalDevice;
import edu.neu.mdims.model.medicaldevice.MedicalDeviceClass;
import edu.neu.mdims.model.medicaldevice.MedicalDeviceSpeciality;
import edu.neu.mdims.model.organizations.MedicalServiceProvider;
import edu.neu.mdims.model.users.Doctor;
import edu.neu.mdims.model.users.Nurse;
import edu.neu.mdims.model.organizations.Network;
import edu.neu.mdims.model.organizations.Warehouse;
import edu.neu.mdims.model.users.NetworkAdmin;
import edu.neu.mdims.model.organizations.Supplier;
import edu.neu.mdims.model.users.SupplierAdmin;
import edu.neu.mdims.model.users.WarehouseManager;
import java.util.Arrays;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * Initialize some data
 *
 * @author NicolasZHANG
 */
public class ApplicationInitializer {

    public static Network initialize() {
        Network network = Network.getInstance();

        // initialize network admin
        NetworkAdmin na = new NetworkAdmin();
        na.setName("Shuai Zhang");
        na.setUserName("admin");
        na.setPassword("123");
        na.setOrganization(network);
        network.getUsers().add(na);

        // initialize MedicalServiceProviders
        MedicalServiceProvider msp1 = new MedicalServiceProvider("Massachusetts General Hospital");
        msp1.setLocation(new GeoPosition(42.363366, -71.068899));

        MedicalServiceProvider msp2 = new MedicalServiceProvider("Brigham and Women's Hospital");
        msp2.setLocation(new GeoPosition(42.336082, -71.107284));

        MedicalServiceProvider msp3 = new MedicalServiceProvider("Brigham and Women's Faulkner Hospital");
        msp3.setLocation(new GeoPosition(42.301884, -71.128981));

        MedicalServiceProvider msp4 = new MedicalServiceProvider("Newton-Wellesley Hospital");
        msp4.setLocation(new GeoPosition(42.331714, -71.245679));

        MedicalServiceProvider msp5 = new MedicalServiceProvider("North Shore Medical Center");
        msp5.setLocation(new GeoPosition(25.861793, -80.213982));

        MedicalServiceProvider msp6 = new MedicalServiceProvider("McLean Hospital");
        msp6.setLocation(new GeoPosition(42.395136, -71.194353));

        MedicalServiceProvider msp7 = new MedicalServiceProvider("Martha's Vineyard Community Hospital");
        msp7.setLocation(new GeoPosition(41.460908, -70.579768));

        MedicalServiceProvider msp8 = new MedicalServiceProvider("Nantucket Cottage Hospital");
        msp8.setLocation(new GeoPosition(41.274945, -70.100533));

        MedicalServiceProvider msp9 = new MedicalServiceProvider("MGH Institute of Health Professions");
        msp9.setLocation(new GeoPosition(42.375057, -71.054073));

        MedicalServiceProvider msp10 = new MedicalServiceProvider("Neighborhood Health Plan");
        msp10.setLocation(new GeoPosition(42.350758, -71.051245));

        MedicalServiceProvider msp11 = new MedicalServiceProvider("Cooley Dickinson Hospital");
        msp11.setLocation(new GeoPosition(42.330725, -72.653193));

        MedicalServiceProvider msp12 = new MedicalServiceProvider("Spaulding Rehabilitation Hospital");
        msp12.setLocation(new GeoPosition(42.379018, -71.049367));

        MedicalServiceProvider msp13 = new MedicalServiceProvider("Partners Community Healthcare Inc.");
        msp13.setLocation(new GeoPosition(42.305032, -71.210857));

        network.getMedicalServiceProviders().addAll(
                Arrays.asList(msp1, msp2, msp3, msp4, msp5, msp6, msp7, msp8, msp9, msp10, msp11, msp12, msp13)
        );

        // initialize doctors and nurses
        int doctorAndNurseCount = 1;
        for (Organization organization : network.getMedicalServiceProviders()) {
            for (int i = 0; i < 10; i++) {
                Doctor doctor = new Doctor();
                doctor.setName("Doctor " + doctorAndNurseCount);
                doctor.setUserName("doc" + doctorAndNurseCount);
                doctor.setPassword("123");
                doctor.setOrganization(organization);

                Nurse nurse = new Nurse();
                nurse.setName("Nurse " + doctorAndNurseCount);
                nurse.setUserName("nurse" + doctorAndNurseCount);
                nurse.setPassword("123");
                nurse.setOrganization(organization);

                network.getUsers().add(doctor);

                network.getUsers().add(nurse);
                doctorAndNurseCount++;
            }
        }

        // initialize suppliers
        Supplier s1 = new Supplier("Philips Healthcare");
        Supplier s2 = new Supplier("Covidien");
        Supplier s3 = new Supplier("Boston Scientific Corp.");
        Supplier s4 = new Supplier("Smith & Nephew Inc.");
        network.getSuppliers().addAll(Arrays.asList(s1, s2, s3, s4));

        // initialize medical devices
        MedicalDevice md1 = new MedicalDevice();
        md1.setName("MobileDiagnost wDR");
        md1.setDescription("Mobile digital radiography system");
        md1.setDeviceClass(MedicalDeviceClass.ClassII);
        md1.setSpeciality(MedicalDeviceSpeciality.Radiology);
        md1.setYearOfManufacture("2014");
        md1.setManufacturer("Philips Healthcare");
        md1.setLotNumber("1");
        md1.setExpectedDeviceLifetime("10 years");
        md1.setSoftwareAndFirmwareVersionNumber("1.0.0");
        md1.setPowerRequirements("40 kW");
        md1.setSerialNumber("712001");
        md1.setPrice(21897);

        MedicalDevice md2 = new MedicalDevice();
        md2.setName("DigitalDiagnost");
        md2.setDescription("Premium digital radiography system");
        md2.setDeviceClass(MedicalDeviceClass.ClassII);
        md2.setSpeciality(MedicalDeviceSpeciality.Radiology);
        md2.setYearOfManufacture("2014");
        md2.setManufacturer("Philips Healthcare");
        md2.setLotNumber("1");
        md2.setExpectedDeviceLifetime("10 years");
        md2.setSoftwareAndFirmwareVersionNumber("1.0.0");
        md2.setPowerRequirements("50 kW, 65 kW, 80 kW");
        md2.setSerialNumber("712220");
        md2.setPrice(76715);

        MedicalDevice md3 = new MedicalDevice();
        md3.setName("Ingenia 3.0T");
        md3.setDescription("MR system");
        md3.setDeviceClass(MedicalDeviceClass.ClassI);
        md3.setSpeciality(MedicalDeviceSpeciality.Radiology);
        md3.setYearOfManufacture("2014");
        md3.setManufacturer("Philips Healthcare");
        md3.setLotNumber("1");
        md3.setExpectedDeviceLifetime("8 years");
        md3.setSoftwareAndFirmwareVersionNumber("1.0.0");
        md3.setPowerRequirements("50 kW, 65 kW, 80 kW");
        md3.setSerialNumber("781342");
        md3.setPrice(102080);

        MedicalDevice md4 = new MedicalDevice();
        md4.setName("Diamond Select Allura Xper FD20");
        md4.setDescription("Interventional X-ray system");
        md4.setDeviceClass(MedicalDeviceClass.ClassII);
        md4.setSpeciality(MedicalDeviceSpeciality.Radiology);
        md4.setYearOfManufacture("2014");
        md4.setManufacturer("Philips Healthcare");
        md4.setLotNumber("5");
        md4.setExpectedDeviceLifetime("10 years");
        md4.setSoftwareAndFirmwareVersionNumber("1.0.0");
        md4.setPowerRequirements("100 to 240 VAC");
        md4.setSerialNumber("889022");
        md4.setPrice(222580);

        s1.getMedicalDevices().addAll(Arrays.asList(md1, md2, md3, md4));

        // initialize supplier admins
        int supplierAdminCount = 1;
        for (Supplier supplier : network.getSuppliers()) {
            SupplierAdmin supplierAdmin = new SupplierAdmin();
            supplierAdmin.setName("Supplier Admin " + supplierAdminCount);
            supplierAdmin.setUserName("sa" + supplierAdminCount);
            supplierAdmin.setPassword("123");
            supplierAdmin.setOrganization(supplier);
            supplierAdminCount++;
        }

        // initialize warehouses
        Warehouse w1 = new Warehouse("Warehouse 1");
        w1.setLocation(new GeoPosition(42.373676, -71.058240));

        Warehouse w2 = new Warehouse("Warehouse 2");
        w2.setLocation(new GeoPosition(42.332891, -71.096962));

        Warehouse w3 = new Warehouse("Warehouse 2");
        w3.setLocation(new GeoPosition(42.380968, -71.120136));

        network.getWarehouses().addAll(Arrays.asList(w1, w2));

        int warehouseAdminCount = 1;
        for (Warehouse warehouse : network.getWarehouses()) {
            WarehouseManager warehouseAdmin = new WarehouseManager();
            warehouseAdmin.setName("Warehouse Admin " + warehouseAdminCount);
            warehouseAdmin.setUserName("wa" + warehouseAdminCount);
            warehouseAdmin.setPassword("123");
            warehouseAdmin.setOrganization(warehouse);
            warehouseAdminCount++;
        }

        return network;
    }
}
