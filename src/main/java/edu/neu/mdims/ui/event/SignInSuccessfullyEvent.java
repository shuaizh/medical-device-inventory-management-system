/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.ui.event;

import edu.neu.mdims.model.users.User;
import java.util.EventObject;

/**
 *
 * @author NicolasZHANG
 */
public class SignInSuccessfullyEvent extends EventObject {

    private final User user;

    public SignInSuccessfullyEvent(Object source, User user) {
        super(source);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

}
