/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.ui.core.notification;

import com.alee.extended.panel.GroupPanel;
import com.alee.managers.notification.NotificationIcon;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotificationPopup;
import javax.swing.JLabel;

/**
 *
 * @author NicolasZHANG
 */
public class NotificationHelper {

    public static void showNotification(NotificationIcon icon, String content, int displayTime) {
        final WebNotificationPopup notificationPopup = new WebNotificationPopup();
        notificationPopup.setIcon(icon);
        notificationPopup.setDisplayTime(displayTime);
        final JLabel label = new JLabel(content);
        notificationPopup.setContent(new GroupPanel(label));
        NotificationManager.showNotification(notificationPopup);
    }
}
