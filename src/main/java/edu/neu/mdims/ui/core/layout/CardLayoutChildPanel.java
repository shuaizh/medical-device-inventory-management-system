package edu.neu.mdims.ui.core.layout;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import edu.neu.mdims.ui.core.layout.animatingcardlayout.AnimatingCardLayout;
import java.awt.CardLayout;
import javax.swing.JPanel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author NicolasZHANG
 */
public abstract class CardLayoutChildPanel extends javax.swing.JPanel {

    protected static final Logger logger = LogManager.getLogger(CardLayoutChildPanel.class.getName());

    protected final JPanel userProcessContainer;

    public CardLayoutChildPanel(JPanel userProcessContainer) {
        this.userProcessContainer = userProcessContainer;
    }

    public void goToPreviousView() {
        userProcessContainer.remove(this);
        AnimatingCardLayout layout = (AnimatingCardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }

    public void goToNextView(CardLayoutChildPanel cardLayoutChildPanel) {
        userProcessContainer.add(cardLayoutChildPanel, cardLayoutChildPanel.getClass().getName());
        AnimatingCardLayout layout = (AnimatingCardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }
}
