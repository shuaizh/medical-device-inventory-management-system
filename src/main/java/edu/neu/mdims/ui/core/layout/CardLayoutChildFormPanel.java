package edu.neu.mdims.ui.core.layout;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import edu.neu.mdims.utils.validation.ValidationForm;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author NicolasZHANG
 */
public abstract class CardLayoutChildFormPanel extends CardLayoutChildPanel {

    private final ValidationForm validationForm = new ValidationForm();

    public CardLayoutChildFormPanel(JPanel userProcessContainer) {
        super(userProcessContainer);
    }

    public ValidationForm getValidationForm() {
        return validationForm;
    }

    public void goToPreviousView() {
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }

    public void goToNextView(CardLayoutChildFormPanel cardLayoutChildPanel) {
        userProcessContainer.add(cardLayoutChildPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }
}
