/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.ui.workspace;

import edu.neu.mdims.model.organizations.Network;
import edu.neu.mdims.model.users.*;
import edu.neu.mdims.ui.core.layout.CardLayoutChildPanel;
import edu.neu.mdims.ui.workspace.doctor.DoctorWorkspace;
import edu.neu.mdims.ui.workspace.medicalserviceprovideradmin.MedicalServiceProviderAdminWorkspace;
import edu.neu.mdims.ui.workspace.networkadmin.NetworkAdminWorkspace;
import edu.neu.mdims.ui.workspace.nurse.NurseWorkspace;
import edu.neu.mdims.ui.workspace.operationmanager.OperationManagerWorkspace;
import edu.neu.mdims.ui.workspace.supplieradmin.SupplierAdminWorkspace;
import edu.neu.mdims.ui.workspace.warehousemanager.WarehouseManagerWorkspace;
import javax.swing.JPanel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author NicolasZHANG
 */
public class WorkspaceFactory {

    static final Logger logger = LogManager.getLogger(WorkspaceFactory.class.getName());

    public static CardLayoutChildPanel generateWorkspace(User user, JPanel userProcessContainer, Network network) {
        if (user instanceof NetworkAdmin) {
            return new NetworkAdminWorkspace(userProcessContainer, network);
        } else if (user instanceof WarehouseManager) {
            return new WarehouseManagerWorkspace(userProcessContainer, network);
        } else if (user instanceof SupplierAdmin) {
            return new SupplierAdminWorkspace(userProcessContainer, network);
        } else if (user instanceof MedicalServiceProviderAdmin) {
            return new MedicalServiceProviderAdminWorkspace(userProcessContainer, network);
        } else if (user instanceof Doctor) {
            return new DoctorWorkspace(userProcessContainer, network);
        } else if (user instanceof Nurse) {
            return new NurseWorkspace(userProcessContainer, network);
        } else if (user instanceof OperationManager) {
            return new OperationManagerWorkspace(userProcessContainer, network);
        } else {
            logger.error("Illegal user type");
            return null;
        }
    }
}
