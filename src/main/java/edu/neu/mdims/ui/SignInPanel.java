/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims.ui;

import com.alee.managers.notification.NotificationIcon;
import edu.neu.mdims.model.organizations.Network;
import edu.neu.mdims.model.users.User;
import edu.neu.mdims.ui.core.layout.CardLayoutChildFormPanel;
import edu.neu.mdims.ui.core.layout.CardLayoutChildPanel;
import edu.neu.mdims.ui.core.notification.NotificationHelper;
import edu.neu.mdims.ui.event.SignInSuccessfullyEvent;
import edu.neu.mdims.ui.event.SignInSuccessfullyEventListener;
import edu.neu.mdims.ui.workspace.WorkspaceFactory;
import edu.neu.mdims.utils.validation.RequiredInputVerifier;
import edu.neu.mdims.utils.validation.ValidationForm;
import java.awt.event.KeyEvent;
import javax.swing.JPanel;

/**
 *
 * @author NicolasZHANG
 */
public class SignInPanel extends CardLayoutChildFormPanel {

    private final Network network;

    /**
     *
     * @param userProcessContainer
     * @param network
     */
    public SignInPanel(JPanel userProcessContainer, Network network) {
        super(userProcessContainer);
        initComponents();
        this.network = network;
        setupFormValidation();
        txtUserName.requestFocus();
    }

    /**
     * Creates new form LoginPanel
     */
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        webLabel3 = new com.alee.laf.label.WebLabel();
        webLabel4 = new com.alee.laf.label.WebLabel();
        btnSignIn = new javax.swing.JButton();
        txtUserName = new javax.swing.JTextField();
        pfPassword = new javax.swing.JPasswordField();
        lbValidationMessage = new javax.swing.JLabel();

        webLabel3.setText("User Name:");

        webLabel4.setText("Password:");

        btnSignIn.setText("Sign In");
        btnSignIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSignInActionPerformed(evt);
            }
        });

        txtUserName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUserNameKeyReleased(evt);
            }
        });

        pfPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pfPasswordKeyReleased(evt);
            }
        });

        lbValidationMessage.setForeground(new java.awt.Color(255, 0, 51));
        lbValidationMessage.setText("Error Message");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(250, 250, 250)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbValidationMessage, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(webLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(webLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnSignIn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtUserName)
                            .addComponent(pfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(24, 24, 24))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addComponent(lbValidationMessage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(webLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUserName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(webLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSignIn)
                .addContainerGap(282, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSignInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSignInActionPerformed
        signIn();
    }//GEN-LAST:event_btnSignInActionPerformed

    private void signIn() {
        if (getValidationForm().isValid()) {
            User user = network.authenticateUserAccount(txtUserName.getText(), pfPassword.getText());
            if (user != null) {
                NotificationHelper.showNotification(NotificationIcon.information, "Sign in successfully!", 3000);

                // call view factory to generate view for the specific role of the user
                CardLayoutChildPanel workspace = WorkspaceFactory.generateWorkspace(user, userProcessContainer, network);
                goToNextView(workspace);
                fireSignInSuccessfullyEvent(new SignInSuccessfullyEvent(this, user));
            } else {
                lbValidationMessage.setText("Wrong credentials, please check your user name and password.");
                txtUserName.setText("");
                pfPassword.setText("");
            }
        }
    }

    private void txtUserNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUserNameKeyReleased
        if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
            signIn();
        }
    }//GEN-LAST:event_txtUserNameKeyReleased

    private void pfPasswordKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pfPasswordKeyReleased
        if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
            signIn();
        }
    }//GEN-LAST:event_pfPasswordKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSignIn;
    private javax.swing.JLabel lbValidationMessage;
    private javax.swing.JPasswordField pfPassword;
    private javax.swing.JTextField txtUserName;
    private com.alee.laf.label.WebLabel webLabel3;
    private com.alee.laf.label.WebLabel webLabel4;
    // End of variables declaration//GEN-END:variables

    private void setupFormValidation() {
        lbValidationMessage.setText("");
        final ValidationForm validationForm = getValidationForm();

        validationForm.addTextField(txtUserName, new RequiredInputVerifier(lbValidationMessage));
        validationForm.addTextField(pfPassword, new RequiredInputVerifier(lbValidationMessage));

    }

    public void addSignInSuccessfullyEventListener(SignInSuccessfullyEventListener listener) {
        listenerList.add(SignInSuccessfullyEventListener.class, listener);
    }

    public void removeSignInSuccessfullyEventListener(SignInSuccessfullyEventListener listener) {
        listenerList.remove(SignInSuccessfullyEventListener.class, listener);
    }

    void fireSignInSuccessfullyEvent(SignInSuccessfullyEvent evt) {
        Object[] listeners = listenerList.getListenerList();
        for (int i = 0; i < listeners.length; i = i + 2) {
            if (listeners[i] == SignInSuccessfullyEventListener.class) {
                ((SignInSuccessfullyEventListener) listeners[i + 1]).signInSuccessfullyEventOccured(evt);
            }
        }
    }

}
