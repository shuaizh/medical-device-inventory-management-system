/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.mdims;

import com.alee.laf.WebLookAndFeel;
import edu.neu.mdims.ui.MainJFrame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author NicolasZHANG
 */
public class Application {

    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(final String[] args) {
        SwingUtilities.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(NimbusLookAndFeel.class.getCanonicalName());
                WebLookAndFeel.initializeManagers();
                final MainJFrame mainJFrame = new MainJFrame();
                mainJFrame.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent we) {
                        // store network data before exitting the application
                        try {
                            mainJFrame.storeNetwork();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e.getMessage(), "Store network error", JOptionPane.ERROR_MESSAGE);
                        } finally {
                            System.exit(0);
                        }
                    }
                });

                mainJFrame.setVisible(true);
            } catch (final ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
                logger.trace(e);
            }
        });
    }
}
